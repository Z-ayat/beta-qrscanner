import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import { COLORS, FONTS, SIZES, assets } from '../constants';

const HomeHeader = ({ title }) => {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Image
        source={assets.HeaderBG}
        resizeMode='cover'
        style={styles.backgroundImage}
      />
      <View style={styles.header}>
        <TouchableOpacity
          style={styles.navButton}
          onPress={() => navigation.goBack()}>
          <Image
            source={assets.ChevronLeft}
            resizeMode='cover'
            style={styles.chevronLeft}
          />
          <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
        <Image source={assets.ShoppingBagIcon} />
      </View>
    </View>
  );
};

const styles = {
  container: {
    backgroundColor: COLORS.header,
    height: 152,
    position: 'relative',
    zIndex: -1,
  },
  backgroundImage: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  header: {
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: SIZES.extraLarge,
  },
  navButton: {
    flexDirection: 'row',
  },
  chevronLeft: {
    marginRight: 20,
  },
  title: {
    color: COLORS.white,
    fontFamily: FONTS.medium,
  },
};

export default HomeHeader;
