import { LinearGradient } from 'expo-linear-gradient';
import { Image } from 'react-native';
import { Alert } from 'react-native';
import { StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { View, Text } from 'react-native';
import { COLORS, FONTS, SIZES } from '../constants';
const PromotionButton = ({
  navigation,
  item,
  title,
  reward,
  btnText,
  btnImg,
  scanDone,
}) => {
  return (
    <View style={styles.promotionButton}>
      <Image
        source={btnImg}
        style={{ width: 28, height: 28 }}
      />
      <View
        style={{
          flex: 3,
          marginLeft: SIZES.large,
        }}>
        <Text style={{ fontFamily: FONTS.medium, color: COLORS.price }}>
          {title}
        </Text>
        <Text style={{ fontFamily: FONTS.regular }}>{reward}</Text>
      </View>
      <TouchableOpacity
        // disabled={scanDone} //if alert is needed ,
        style={{
          flex: 1.5,
        }}
        onPress={() => {
          if (scanDone) return Alert.alert('Already Scanned');
          navigation.replace('QrScreen', { item });
        }}>
        <LinearGradient
          colors={scanDone ? ['#F5F7FE', '#F5F7FE'] : ['#00E8DB', '#5C4CDB']}
          start={{ x: 0, y: 1 }}
          end={{ x: 1, y: 0 }}
          style={{
            borderRadius: SIZES.font,
            paddingVertical: SIZES.font,
          }}>
          <Text
            style={{
              textAlign: 'center',
              color: scanDone ? 'green' : COLORS.white,
              fontFamily: FONTS.medium,
            }}>
            {btnText}
          </Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};
export default PromotionButton;

const styles = StyleSheet.create({
  promotionButton: {
    marginVertical: SIZES.medium,
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
