import { useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import { useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import { View, Text, StyleSheet } from 'react-native';
import { useDispatch } from 'react-redux';
import { COLORS, FONTS, SIZES } from '../constants';
import { setScanned } from '../store/qrScannerSlice';
const CongratsComp = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  useEffect(() => {
    // Set flag to prevent going back
    navigation.setOptions({
      headerLeft: () => null, // hide the back button
      gestureEnabled: false, // disable swipe back gesture
    });

    // Unset flag when component disappears
    return () => {
      dispatch(setScanned(false));
      navigation.setOptions({
        headerLeft: undefined, // show the back button
        gestureEnabled: true, // enable swipe back gesture
      });
    };
  }, []);
  return (
    <TouchableOpacity
      onPress={() => dispatch(setScanned(false))}
      style={styles.scannedOverlay}>
      <LinearGradient
        colors={['#00E8DB', '#5C4CDB']}
        start={{ x: 0, y: 1 }}
        end={{ x: 1, y: 0 }}
        style={{
          borderRadius: 10000,
          borderWidth: 18,
          borderColor: 'rgba(48, 48, 48, 0.6)',
        }}>
        <View style={styles.scannedPointsContainer}>
          <Text style={styles.scannedPoints}>100</Text>
          <Text style={styles.scannedPointsText}>Points</Text>
        </View>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  scannedOverlay: {
    position: 'absolute',
    backgroundColor: 'rgba(0,0,0,0.4)',
    width: '100%',
    height: '100%',
    zIndex: 999999,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scannedPointsContainer: {
    padding: SIZES.extraLarge,
    width: '40%',
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scannedPoints: {
    fontSize: SIZES.extraLarge,
    fontFamily: FONTS.bold,
    color: COLORS.white,
  },
  scannedPointsText: {
    fontSize: SIZES.font,
    fontFamily: FONTS.regular,
    color: COLORS.white,
  },
});
export default CongratsComp;
