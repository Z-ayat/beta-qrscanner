import { useNavigation } from '@react-navigation/core';
import { useRef } from 'react';
import { StyleSheet, Text } from 'react-native';
import { Alert } from 'react-native';
import { Image, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';
import { assets } from '../constants';
import { ButtonWithTextAndIcon } from './Button';
export default function CardComponent({ item }) {
  const navigation = useNavigation();
  const { scanned } = useSelector((state) => state.qrScanner);
  const scanDone = scanned.includes(item.barcode);
  const onQrButtonPress = () => {
    if (scanDone) return Alert.alert('Already Scanned');
    navigation.navigate('QrScreen', { item });
  };

  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('Details', { item });
      }}
      pointerEvents='box-none'
      style={styles.cardComponent}>
      <View style={{ flexDirection: 'row' }}>
        <Image
          src={`https://develop.yeshtery.com/files/${item.image_url}`}
          style={{ width: 110, height: 110 }}
        />
        <View
          style={{
            justifyContent: 'space-between',
            marginLeft: 10,
          }}>
          <Text
            style={{ fontSize: 16, maxWidth: 200 }}
            numberOfLines={2}>
            {item.name}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              gap: 10,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <ButtonWithTextAndIcon
              iconImg={assets.QrIcon}
              onPressHandler={onQrButtonPress}
              scanDone={scanDone}
              text={scanDone ? 'Done' : '120'}
            />
            <ButtonWithTextAndIcon
              iconImg={assets.RecipetIcon}
              text='200'
            />
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cardComponent: {
    height: 130,
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
    borderRadius: 8,
    backgroundColor: '#fff',
    shadowColor: 'rgba(2, 104, 211, 0.08)',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 1,
    shadowRadius: 8,
    elevation: 8,
    marginBottom: 20,
  },
});
