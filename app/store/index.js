import { configureStore } from '@reduxjs/toolkit';
import qrScanner from './qrScannerSlice';
import products from './productSlice';
export default store = configureStore({
  reducer: { qrScanner, products },
});
