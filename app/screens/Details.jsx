import { useEffect } from 'react';
import { View, Text, ScrollView, FlatList } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ColorsButton, HomeHeader, SizeButton } from '../components';
import CongratsComp from '../components/CongratsComp';
import ImageSwiper from '../components/ImageSwiper';
import PromotionButton from '../components/PromotionButton';
import { assets } from '../constants';
import { colorButtons, sizeButtons } from '../constants/dummy';
import { cleanRecords, fetchProduct } from '../store/productSlice';
import { styles } from './DetailsScreenStyles';
const Details = ({ route, navigation }) => {
  const { item } = route.params;
  const { isScanned, scanned } = useSelector((state) => state.qrScanner);
  const { product, loading } = useSelector((state) => state.products);
  const scanDone = scanned.includes(item.barcode);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProduct(item.id));
    return () => {
      dispatch(cleanRecords());
    };
  }, [dispatch]);

  return (
    <View style={{ flex: 1 }}>
      <HomeHeader title='Product Details' />
      {isScanned ? <CongratsComp /> : null}
      <ScrollView style={styles.scrollContainer}>
        {/* Swiper Here */}
        <ImageSwiper
          product={product}
          loading={loading}
        />
        {/* Swiper Here */}
        <View style={styles.detailsContainer}>
          <Text style={styles.productName}>{product.name}</Text>
          <View style={styles.productPriceContainer}>
            <Text style={styles.productPrice}>({product.price} EGP)</Text>
            <Text style={styles.productOldPrice}>{product.price * 2} EGP</Text>
          </View>
        </View>
        <Text style={styles.productDescription}>{product.description}</Text>
        <View>
          <Text style={styles.colorText}>color</Text>
          <View style={styles.colorButtonContainer}>
            {colorButtons.map((color, idx) => (
              <ColorsButton
                key={++idx}
                color={color}
              />
            ))}
          </View>
        </View>
        <View>
          <Text style={styles.sizeButtonContainer}>size</Text>
          <View style={styles.colorButtonContainer}>
            {sizeButtons.map((size, idx) => (
              <SizeButton
                key={++idx}
                size={size}
              />
            ))}
          </View>
        </View>
        <View style={styles.promotionsContainer}>
          <PromotionButton
            item={item}
            navigation={navigation}
            product={product}
            btnImg={assets.QrIcon}
            scanDone={scanDone}
            title='Scan'
            reward='& get 100 points'
            isScanned={isScanned}
            btnText={scanDone ? 'Done' : 'Scan'}
          />
          <PromotionButton
            navigation={navigation}
            product={product}
            btnImg={assets.RecipetIcon}
            title='Buy & Submit'
            reward='the receipt for 120 points'
            btnText='Submit'
          />
        </View>
      </ScrollView>
    </View>
  );
};
export default Details;
