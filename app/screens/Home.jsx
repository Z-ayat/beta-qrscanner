import React, { useEffect } from 'react';
import {
  View,
  FlatList,
  Text,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';

import { CardComponent, HomeHeader } from '../components';
import { COLORS, FONTS, SIZES } from '../constants';
import { useDispatch, useSelector } from 'react-redux';
import { fetchProducts } from '../store/productSlice';

const Home = () => {
  const dispatch = useDispatch();
  const { records: products, loading } = useSelector((state) => state.products);
  useEffect(() => {
    dispatch(fetchProducts());
  }, [dispatch]);

  return (
    <View style={styles.container}>
      <HomeHeader title='Scan Products' />

      <View style={styles.content}>
        {!products ? (
          <SafeAreaView style={styles.center}>
            <Text
              style={{
                fontFamily: FONTS.bold,
                fontSize: SIZES.extraLarge,
                color: COLORS.header,
              }}>
              Sorry there is no data yet ...
            </Text>
          </SafeAreaView>
        ) : loading ? (
          <SafeAreaView style={styles.center}>
            <ActivityIndicator />
          </SafeAreaView>
        ) : (
          <FlatList
            data={products}
            renderItem={({ item }) => <CardComponent item={item} />}
            keyExtractor={(item) => item.id}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.listContent}
          />
        )}
      </View>
    </View>
  );
};

const styles = {
  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
  },
  content: {
    backgroundColor: COLORS.white,
    borderTopLeftRadius: SIZES.extraLarge,
    borderTopRightRadius: SIZES.extraLarge,
    flex: 1,
    marginTop: -50,
    paddingHorizontal: SIZES.extraLarge,
    paddingTop: SIZES.extraLarge,
  },
  listContent: {
    // paddingBottom: 50,
  },
};

export default Home;
