export const COLORS = {
  header: 'rgb(49, 0, 126)',
  primary: '#090A0A',
  price: '#1F54CF',
  secondary: '#090A0A99',
  white: '#FFF',
  gray: '#9C9C9C',
};

export const SIZES = {
  base: 8,
  small: 12,
  font: 14,
  medium: 16,
  large: 18,
  extraLarge: 24,
};

export const FONTS = {
  light: 'RobotoLight',
  regular: 'RobotoRegular',
  medium: 'RobotoMedium',
  semiBold: 'RobotoSemiBold',
  bold: 'RobotoBold',
};

export const SHADOWS = {
  light: {
    shadowColor: COLORS.gray,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
  },
  medium: {
    shadowColor: COLORS.gray,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.29,
    shadowRadius: 4.65,

    elevation: 7,
  },
  dark: {
    shadowColor: COLORS.gray,
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.41,
    shadowRadius: 9.11,

    elevation: 14,
  },
};
