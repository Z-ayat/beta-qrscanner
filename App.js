import { DefaultTheme, NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { useFonts } from 'expo-font';
import Home from './app/screens/Home';
import Details from './app/screens/Details';
import QrScreen from './app/screens/QrScreen';
import { Provider } from 'react-redux';
import store from './app/store';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    backgroundColor: 'transparent',
  },
};
const Stack = createStackNavigator();
export default function App() {
  const [loaded] = useFonts({
    RobotoBold: require('./app/assets/fonts/Poppins-Bold.ttf'),
    RobotoSemiBold: require('./app/assets/fonts/Poppins-SemiBold.ttf'),
    RobotoMedium: require('./app/assets/fonts/Poppins-Medium.ttf'),
    RobotoRegular: require('./app/assets/fonts/Poppins-Regular.ttf'),
    RobotoLight: require('./app/assets/fonts/Poppins-Light.ttf'),
  });
  if (!loaded) {
    return null;
  }
  return (
    <NavigationContainer theme={theme}>
      <Provider store={store}>
        <Stack.Navigator
          screenOptions={{ headerShown: false }}
          initialRouteName='Home'>
          <Stack.Screen
            name='Home'
            component={Home}
          />
          <Stack.Screen
            name='Details'
            component={Details}
          />
          <Stack.Screen
            name='QrScreen'
            component={QrScreen}
          />
        </Stack.Navigator>
      </Provider>
    </NavigationContainer>
  );
}
